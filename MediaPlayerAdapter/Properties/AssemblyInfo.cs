﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MediaPlayerAdapter")]
[assembly: AssemblyDescription("Youbora adapter for MediaPlayer")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Nice People At Work")]
[assembly: AssemblyProduct("MediaPlayerAdapter")]
[assembly: AssemblyCopyright("Copyright © 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("6.0.4.0")]
[assembly: AssemblyFileVersion("6.0.4.0")]
[assembly: ComVisible(false)]