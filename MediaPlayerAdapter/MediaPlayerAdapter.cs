﻿using Windows.Media.Playback;
using Youbora.adapter;
using Youbora.log;
using Youbora.timer;
using System;
using System.Collections.Generic;

namespace Youbora.MediaPlayerAdapter
{
    public class MediaPlayerAdapter : Adapter
    {
        private Timer playheadTimer;
        private double lastPlayheadValue = 0d;
        private long lastResumeTime = 0L;
        private String seekBeginEvent = String.Empty;
        private String bufferBeginEvent = String.Empty;

        #region constructor
        public MediaPlayerAdapter(object playerref) : base(playerref) { }

        public override void RegisterListeners()
        {
            // Configure timer
            playheadTimer = new Timer(PlayHeadTimerFunction, 500);

            // Start Buffer Monitor
            Monitorplayhead(false, false);

            if (player != null)
            {
                // Register events
                ((MediaPlayer)this.player).MediaOpened += Player_MediaOpened;
                ((MediaPlayer)this.player).CurrentStateChanged += Player_CurrentStateChanged;
                ((MediaPlayer)this.player).MediaFailed += Player_MediaFailed;
                ((MediaPlayer)this.player).MediaEnded += Player_MediaEnded;
                ((MediaPlayer)this.player).BufferingStarted += Player_BufferingStarted;
                ((MediaPlayer)this.player).BufferingEnded += Player_BufferingEnded;
                ((MediaPlayer)this.player).SeekCompleted += Player_SeekCompleted;
                ((MediaPlayer)this.player).SourceChanged += Player_SourceChanged;
            }
        }

        public override void UnregisterListeners()
        {
            // UnRegister events
            if (player != null)
            {
                ((MediaPlayer)this.player).MediaOpened -= Player_MediaOpened;
                ((MediaPlayer)this.player).CurrentStateChanged -= Player_CurrentStateChanged;
                ((MediaPlayer)this.player).MediaFailed -= Player_MediaFailed;
                ((MediaPlayer)this.player).MediaEnded -= Player_MediaEnded;
                ((MediaPlayer)this.player).BufferingStarted -= Player_BufferingStarted;
                ((MediaPlayer)this.player).BufferingEnded -= Player_BufferingEnded;
                ((MediaPlayer)this.player).SeekCompleted -= Player_SeekCompleted;
                ((MediaPlayer)this.player).SourceChanged -= Player_SourceChanged;
            }

            // Stop timer
            playheadTimer.Stop();
        }
        #endregion
        #region eventHandlers
        private void PlayHeadTimerFunction(object dt)
        {
            try
            {
                this.lastPlayheadValue = this.GetPlayhead();
            }
            catch (Exception) { }
        }

        private void Player_MediaOpened(MediaPlayer sender, object e)
        {
            Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " MediaOpened");
        }

        private void Player_BufferingStarted(MediaPlayer sender, object e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " BufferingStarted :: Position: " + this.GetPlayhead());

                if (!flags.isStarted)
                {
                    FireStart(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "BufferingStarted" }) } });
                    playheadTimer.Start();
                }

                if (flags.isJoined && !flags.isPaused)
                {
                    if (Math.Abs(this.GetPlayhead() - lastPlayheadValue) > 1d)
                    {
                        FireSeekBegin();
                        seekBeginEvent = "BufferingStarted";
                    }
                    else
                    {
                        FireBufferBegin();
                        bufferBeginEvent = "BufferingStarted";
                    }
                }
                else
                {
                    Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " Ignore buffer begin if is on paused");
                }

                // Update last value
                this.lastPlayheadValue = this.GetPlayhead();
            }
            catch (Exception) { }
        }

        private void Player_BufferingEnded(MediaPlayer sender, object e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " BufferingEnded :: Position: " + this.GetPlayhead());

                if (flags.isJoined)
                {
                    if ((DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastResumeTime) > 250)
                    {
                        FireBufferEnd(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { bufferBeginEvent, "BufferingEnded" }) } });
                        bufferBeginEvent = String.Empty;
                    }
                    else
                    {
                        CancelBuffer();
                        bufferBeginEvent = String.Empty;
                        Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " [!!] Detected a false buffer after resume event");
                    }
                }
                else
                {
                    FireJoin(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "BufferingEnded" }) } });
                }

                // Update last value
                this.lastPlayheadValue = this.GetPlayhead();
            }
            catch (Exception) { }
        }

        private void Player_SeekCompleted(MediaPlayer sender, object e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " SeekCompleted :: Position: " + this.GetPlayhead());

                if (flags.isJoined)
                {
                    if (flags.isSeeking)
                    {
                        FireSeekEnd(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { seekBeginEvent, "SeekCompleted" }) } });
                        seekBeginEvent = String.Empty;
                    }
                    else
                    {
                        Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " [!!] Seek not detected on seek completed");
                        FireSeekBegin();
                        FireSeekEnd(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "SeekCompleted", "SeekCompleted" }) } });
                        seekBeginEvent = String.Empty;
                    }
                }
            }
            catch (Exception) { }
        }

        private void Player_CurrentStateChanged(MediaPlayer sender, object e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " State: " + ((MediaPlayer)this.player).PlaybackSession.PlaybackState.ToString());

                if (((MediaPlayer)this.player).PlaybackSession.PlaybackState == MediaPlaybackState.Opening)
                {
                    // We can't trigger start here, because views without autoplay is not working correctly
                }
                else if (((MediaPlayer)this.player).PlaybackSession.PlaybackState == MediaPlaybackState.Paused)
                {
                    FirePause(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "CurrentStatePaused" }) } });
                }
                else if (((MediaPlayer)this.player).PlaybackSession.PlaybackState == MediaPlaybackState.Buffering)
                {
                    // Buffer is controlled in the buffering events
                }
                else if (((MediaPlayer)this.player).PlaybackSession.PlaybackState == MediaPlaybackState.Playing)
                {
                    if (!flags.isStarted)
                    {
                        FireStart(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "CurrentStatePlaying" }) } });
                        playheadTimer.Start();
                    }

                    if (flags.isPaused)
                    {
                        FireResume(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "CurrentStatePlaying" }) } });
                        this.lastResumeTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                    }
                }
            }
            catch (Exception) { }
        }

        private void Player_MediaFailed(MediaPlayer sender, MediaPlayerFailedEventArgs e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " Media Failed; Fire error!!");

                if (e.ErrorMessage != null && String.IsNullOrEmpty(e.ErrorMessage.ToString()))
                {
                    if (e.ExtendedErrorCode != null)
                    {
                        FireError(e.Error.ToString(), e.ExtendedErrorCode.ToString());
                    }
                    else
                    {
                        FireError(e.Error.ToString());
                    }
                }
                else
                {
                    if (e.ExtendedErrorCode != null)
                    {
                        FireError(e.ErrorMessage.ToString(), e.ExtendedErrorCode.ToString());
                    }
                    else
                    {
                        FireError(e.ErrorMessage.ToString());
                    }
                }

                if (flags.isStopped)
                {
                    playheadTimer.Stop();
                }
            }
            catch (Exception) { }
        }

        private void Player_SourceChanged(MediaPlayer sender, object e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " Source Changes; Fire stop!!");

                FireStop(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "SourceChanged" }) } });
                playheadTimer.Stop();
            }
            catch (Exception) { }
        }

        private void Player_MediaEnded(MediaPlayer sender, object e)
        {
            try
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " Media Ended; Fire stop!!");

                FireStop(new Dictionary<String, Object> { { "triggeredEvents", TriggeredEventsToString(new String[] { "MediaEnded" }) } });
                playheadTimer.Stop();
            }
            catch (Exception) { }
        }

        private String TriggeredEventsToString(String[] triggeredEvents)
        {
            try
            {
                if (triggeredEvents != null)
                {
                    return "[\"" + String.Join("\",\"", triggeredEvents) + "\"]";
                }
            }
            catch (Exception) {}

            return String.Empty;
        }

        #endregion
        #region getters
        public override string GetVersion()
        {
            return "6.0.4-MediaPlayer-adapter-csharp";
        }

        public override double GetPlayhead()
        {
            try
            {
                return ((MediaPlayer)this.player).PlaybackSession.Position.TotalSeconds;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public override double GetPlayrate()
        {
            try
            {
                if (flags.isPaused) return 0;
                return (double)((MediaPlayer)this.player).PlaybackSession.PlaybackRate;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public override double? GetDuration()
        {
            try
            {
                return ((MediaPlayer)this.player).PlaybackSession.NaturalDuration.TotalSeconds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override String GetResource()
        {
            try
            {
                if (((MediaPlayer)this.player).Source.GetType() == typeof(MediaPlaybackItem))
                {
                    return ((MediaPlaybackItem)((MediaPlayer)this.player).Source).Source.Uri.ToString();
                }
            }
            catch (Exception)
            {
                Log.Debug(DateTime.Now.ToString("hh:mm:ss.fff tt") + " Can't get media resource");
            }

            return null;
        }

        public override string GetPlayerVersion()
        {
            var p = Windows.ApplicationModel.Package.Current.Id.Version;
            return p.Major + "." + p.Minor + "." + p.Build;
        }

        public override string GetPlayerName()
        {
            return "MediaPlayer";
        }
        #endregion
    }
}
